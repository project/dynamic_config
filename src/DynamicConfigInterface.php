<?php

namespace Drupal\dynamic_config;

/**
 * Interface DynamicConfigInterface.
 *
 * @package Drupal\dynamic_config
 */
interface DynamicConfigInterface {

  /**
   * @return array
   */
  public function getConfig(): array;

}
