<?php

namespace Drupal\dynamic_config\Plugin\ConfigFilter;

use Drupal\Component\Utility\NestedArray;
use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dynamic_config\DynamicConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config filter to add dynamic configurations based on yaml definitions.
 *
 * @ConfigFilter(
 *   id = "dynamic_config_filter",
 *   label = "Dynamic Config Filter",
 *   weight = 5
 * )
 */
class DynamicConfigFilter extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\dynamic_config\DynamicConfigInterface
   */
  private $dynamicConfig;

  /**
   * DynamicConfigFilter constructor.
   *
   * @param array                                         $configuration
   * @param                                               $plugin_id
   * @param                                               $plugin_definition
   * @param \Drupal\dynamic_config\DynamicConfigInterface $dynamic_config
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DynamicConfigInterface $dynamic_config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->dynamicConfig = $dynamic_config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('dynamic_config')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {
    if (!$this->source->getCollectionName()) {
      $config = $this->dynamicConfig->getConfig();
      if (!empty($config[$name])) {
        $this->recursiveCallback($config[$name], $name, $data);
        $data = NestedArray::mergeDeep($data, $config[$name]);
      }
    }

    return $data;
  }

  /**
   * @param array $array
   * @param $name
   * @param $data
   */
  private function recursiveCallback(array &$array, $name, $data): void {
    foreach ($array as $key => &$value) {
      if (is_array($value)) {
        $this->recursiveCallback($array, $name, $data);
      }
      elseif (is_callable($value)) {
        $value = $value($name, $data);
      }
      else {
        unset($array[$key]);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data) {
    if (!$this->source->getCollectionName()) {
      $config = $this->dynamicConfig->getConfig();
      foreach ($config as $config_name => $values) {
        if (in_array($config_name, $names)) {
          $data[$config_name] = $this->filterRead($config_name, $data[$config_name]);
        }
      }
    }

    return $data;
  }

}
