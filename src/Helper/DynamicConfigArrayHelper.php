<?php

namespace Drupal\dynamic_config\Helper;

/**
 * Class DynamicConfigArrayHelper.
 *
 * @package Drupal\dynamic_config\Helper
 */
class DynamicConfigArrayHelper {

  /**
   * @param array  $array
   * @param array  $prefixes
   * @param array  $suffixes
   * @param string $glue
   *
   * @return array
   */
  public static function wrapItems(array $array, array $prefixes = [], array $suffixes = [], $glue = ':'): array {
    $wrap = [
      'prefixes' => $prefixes,
      'suffixes' => $suffixes,
      'glue' => $glue,
    ];
    array_walk($array, function(&$item, $key, $wrap) {
      $pieces = array_merge($wrap['prefixes'], [$item], $wrap['suffixes']);
      $item = implode($wrap['glue'], $pieces);
    }, $wrap);
    return $array;
  }


}
