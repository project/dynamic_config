<?php

namespace Drupal\dynamic_config;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class DynamicConfig.
 *
 * @package Drupal\dynamic_config
 */
class DynamicConfig implements DynamicConfigInterface {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * @var \Drupal\dynamic_config\DynamicConfigYamlDiscovery
   */
  private $dynamicConfigYamlDiscovery;

  /**
   * @var array
   */
  private $config = [];

  /**
   * @var array
   */
  private $configGrupedByModule = [];

  /**
   * @param \Drupal\Core\Extension\ModuleHandlerInterface  $module_handler
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
    $this->scan();
    $this->process();
  }

  /**
   * Scan all dynamic config yaml files.
   */
  private function scan(): void {
    $this->dynamicConfigYamlDiscovery = new DynamicConfigYamlDiscovery($this->moduleHandler->getModuleDirectories());
    $this->configGrupedByModule = $this->dynamicConfigYamlDiscovery->findAll();
  }

  /**
   * Processs scaned config.
   */
  private function process(): void {
    foreach ($this->configGrupedByModule as $module => $config_files) {
      foreach ($config_files as $config_name => $value) {
        if (empty($this->config[$config_name])) {
          $this->config[$config_name] = [];
        }
        $this->config[$config_name] = NestedArray::mergeDeep($this->config[$config_name], $value);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(): array {
    return !empty($this->config)
      ? $this->config
      : [];
  }

}
