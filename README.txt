This module allow you define callbacks for specified config items using YAML
config files located in modules.

HOW TO USE:
Create dynamic_config directory in your module and put your config files
there.

Those config items will be calculated and merged in the import process with
your config/sync configurations to get the active configuration.

EXAMPLE:
config/sync/simple_sitemap.settings.yml:
----------------------------------------
base_url: ''

my_module/dynamic_config/simple_sitemap.settings.yml:
-----------------------------------------------------
base_url: '\Drupal\my_module\SimpleSitemapBaseUrlDynamicConfig::getConfig'

my_module/SimpleSitemapBaseUrlDynamicConfig.php
----------------------------------------
<?php

namespace Drupal\my_module\DynamicConfig;

/**
 * Class SimpleSitemapBaseUrlDynamicConfig.
 *
 * @package Drupal\my_module\DynamicConfig
 */
class SimpleSitemapBaseUrlDynamicConfig {

  /**
   * Dynamic config item value calculation.
   *
   * @param string $name
   *   Config object name.
   * @param array $data
   *   Config object values.
   *
   * @return mixed
   */
  public function getConfig($name, $data) {
    if ('foo' === 'bar') {
      return 'http://example.com';
    }
    return 'https://example.com';
  }

}

active config:
--------------
base_url: 'https://example.com'
